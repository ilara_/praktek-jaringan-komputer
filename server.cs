﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpSrvr
{
    public static void Main()
    {
        int recv;
        string ch, user;
        string input;
        byte[] username = new byte[1024];
        byte[] data = new byte[1024];
        IPAddress ipAd = IPAddress.Parse("0.0.0.0");
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8080);


        Socket newsock = new Socket(AddressFamily.InterNetwork,
                        SocketType.Dgram, ProtocolType.Udp);
        //listening 
        newsock.Bind(ipep);
        Console.WriteLine("Waiting for a client...");

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
        EndPoint Remote = (EndPoint)(sender);

        recv = newsock.ReceiveFrom(data, ref Remote);

        Console.WriteLine("Message received from {0}", Remote.ToString());
        Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

        string welcome = "welcome to my server";
        data = Encoding.ASCII.GetBytes(welcome);
        newsock.SendTo(data, data.Length, SocketFlags.None, Remote);

        //terima username
        username = new byte[1024];
        int terima = newsock.ReceiveFrom(username, ref Remote);
        ch = Encoding.ASCII.GetString(username, 0, terima);
        Console.WriteLine("username client : " + Encoding.ASCII.GetString(username, 0, terima));

        //kirim
        Console.Write("input username : ");
        user = Console.ReadLine();
        newsock.SendTo(Encoding.ASCII.GetBytes(user), Remote);

        Console.WriteLine("mulai Chat!");
        while (true)
        {
            //terima
            data = new byte[1024];
            recv = newsock.ReceiveFrom(data, ref Remote);
            Console.WriteLine(ch + " : " + Encoding.ASCII.GetString(data, 0, recv));
            //kirim
            Console.Write(user + " : ");
            input = Console.ReadLine();
            newsock.SendTo(Encoding.ASCII.GetBytes(input), Remote);
        }
    }
}
